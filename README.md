TODO: Update linux related info to mac os
+ **WM**: [Bspwm](https://github.com/awesomeWM/awesome/)
+ **OS**: [RebornOS](https://rebornos.org/) (Arch base distro)
+ **Shell**: [Zsh](https://wiki.archlinux.org/index.php/Zsh)
+ **Terminal**: [Alacritty](https://github.com/alacritty/alacritty)
+ **Editor**: [VS Code](https://code.visualstudio.com/), [Neovim](https://github.com/neovim/neovim/)
+ **File Manager**: [Thunar](https://git.xfce.org/xfce/thunar/)
+ **Launcher**: [Rofi](https://github.com/davatorium/rofi/)
+ **Browser**: [Firefox](https://www.mozilla.org/en-US/firefox/new/)

