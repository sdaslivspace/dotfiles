export BROWSER=/usr/bin/firefox
export EDITOR='nvim'
export STORAGE=$HOME/str
export STARSHIP_CONFIG=$ZSH_CONFIG/starship.toml
export DOT_DIR=$HOME/Dotfiles/config/

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
export PATH=$PATH:$HOME/Dotfiles/bin:$HOME/.local/bin

export HYPHEN_INSENSITIVE="true"
export COMPLETION_WAITING_DOTS="true"

export ZDOTDIR=$HOME/.cache/zsh/
export HIST_STAMPS="dd.mm.yyyy"
export HISTFILE=$HOME/.cache/zsh/history
export HISTSIZE=1000
export SAVEHIST=1000

export FIREFOX_DIR="/home/soma/.mozilla/firefox/v1n033gs.default-release-1612686329958"

export KEYTIMEOUT=1
export QT_AUTO_SCREEN_SCALE_FACTOR=0 zeal

export LANG=en_US.UTF-8
