# zgen load junegunn/fzf shell
# zgen load zsh-users/zsh-autosuggestions

export OHMYZSH=$HOME/.config/oh-my-zsh

plugins=(gh aliases git zsh-syntax-highlighting zsh-autosuggestions)
source $OHMYZSH/oh-my-zsh.sh